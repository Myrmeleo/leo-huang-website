//required
export const dynamic = 'force-dynamic';

import './styles.css';
import CommentSection from "@/components/comment/section";
import Container from "@/components/container";
import { blogDateFormat } from "@/lib/formatDate";
import { getAllPosts, getPostBySlug } from "@/lib/getPost";
import markdownToHtml from "@/lib/markdownToHtml";
import { Metadata } from "next";
import capitalizeString from "@/lib/capitalizeString";
import { getSession } from "@auth0/nextjs-auth0";

export async function generateMetadata(
  { params }
): Promise<Metadata> {
  const slug = params.slug as string;
  const realTitle = capitalizeString(slug.replaceAll("-", " "));
  return {
    title: realTitle
  }
}

export default async function PostPage({ params }: { params: { slug: string } }) {
  const { slug } = params;
  const { post } = await getPostHTML(slug);
  
  //pages can be async but components cannot, so fetch user here and pass down
  const session = await getSession();
  const user = session ? session.user : null;

  return (
    <div
      className="min-h-screen bg-[url('/longtan-park.jpg')] bg-contain bg-top py-20"
      title="Longtan Park in Liuzhou, China. There are dozens of mountains in the area that one can climb."
    >
      <Container>
        <article className="bg-violet-950 p-10 text-white">
          <header>
            <h1 className="font-bold">{post.title}</h1>
            {post.excerpt ? (
              <p className="my-1 text-xl">{post.excerpt}</p>
            ) : null}
            <time className="flex my-1 text-amber-200">
              {blogDateFormat(new Date(post.date))}
            </time>
          </header>
          <div
            className="mt-7"
            dangerouslySetInnerHTML={{ __html: post.content }}
          />
        </article>
        {<CommentSection user={user}/>}
      </Container>
    </div>
  );
}

//we need post content in HTML format, so convert from markdown format and return
async function getPostHTML(slug) {
  const post = getPostBySlug(slug, [
    "slug",
    "title",
    "excerpt",
    "date",
    "content",
  ]);
  const content = await markdownToHtml(post.content || "");

  return {
    post: {
      ...post,
      content,
    },
  };
}

//take all post names from _posts and use each as a value for dynamic page generation
export async function generateStaticParams() {
  const posts = getAllPosts(["slug"]);
  return posts.map((post) => {
    return post.slug;
  });
}
