import Link from "next/link";
import Container from "@/components/container";
import { distanceToNow } from "@/lib/formatDate";
import { getAllPosts } from "@/lib/getPost";
import { Metadata } from "next"

export const metadata: Metadata = {
  title: 'Posts',
  description: 'The list of Leo\'s blog posts.',
}

export default function NotePage() {
  const allPosts = getAllPosts(["slug", "title", "excerpt", "date"]);
  return (
    <div
    className="min-h-screen bg-[url('/golden-temple.jpg')] bg-cover bg-top bg-no-repeat pt-10"
    title="Kinkaku-Ji (Golden Pavilion Temple) in Kyoto, Japan. A Buddhist temple overlaid in gold. The temple itself is a novelty, but the surrounding gardens account for most of the beauty of the place."
    >
      <Container>
        {allPosts.length ? (
          allPosts.map((post) => (
            <article key={post.slug} className="bg-rose-600/80 hover:bg-rose-700 rounded-l p-5 my-5">
              {/*pt in the Link is non-functioning so I have to use this*/}
              <div/>
              <Link
                href={`/posts/${post.slug}`}
                className="text-lg font-bold text-white"
              >
                {post.title}
              </Link>
              <p className="text-white">
                {post.excerpt}
              </p>
              <div className="text-amber-200">
                <time>{distanceToNow(new Date(post.date))}</time>
              </div>
            </article>
          ))
        ) : (
          <div className="bg-cyan-600/80 bg-clip-content rounded-2xl p-5">
            <p className="text-white">
              I'll put something here eventually.
            </p>
          </div>
        )}
      </Container>
    </div>
  );
}
