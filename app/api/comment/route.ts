import createComment from "@/lib/api/createComment";
import deleteComment from "@/lib/api/deleteComment";
import fetchComment from "@/lib/api/fetchComments";

export async function GET(request: Request) {
    return fetchComment(request);
}

export async function POST(request: Request) {
    return createComment(request);
}

export async function DELETE(request: Request) {
    return deleteComment(request);
}