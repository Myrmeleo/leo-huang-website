import Container from "@/components/container";
import { Metadata } from "next"
import { getLatestPosts } from "@/lib/getPost";
import { distanceToNow } from "@/lib/formatDate";
import Link from "next/link";

export const metadata: Metadata = {
  title: 'Leo\'s Archive',
  description: 'A collection of ideas and creations by Leo Huang concerning fitness, philosophy, and whatever else he wants to say.',
}

function HomePage() {
  const numPostsToDisplay = 3;
  const latestPosts = getLatestPosts(numPostsToDisplay);
  return (
    <div
      className="min-h-screen bg-[url('/lakeside-cliff.jpg')] bg-cover bg-top bg-no-repeat pt-10"
      title=""
    >
      <Container>
        <div className="space-y-5 bg-sky-600/80 rounded-xl p-7 text-white">
          <p>
            Welcome to my sanctuary of thoughts. I'm Leo, a bodybuilder, philosophy hobbyist, and outdoor adventurer. This website will be used to document my journey though life as I happen upon new ideas and activities.
            I have many grand ambitions, and I hope the challenge of achieving them will make a long and fulfilling life. There is the struggle against the world, and there is the struggle to overcome my own weaknesses.
            Hopefully I'll learn many things, and it'll be entertaining to follow.
          </p>
          <p>
            I plan to post philosophical essays, bodybuilding advice, and whatever else I encounter that fascinates me and is worth sharing with the world. How many people my content reaches doesn't really concern me, so long as
            I can help another person walk their own path to excellence.
          </p>
          <div>
            <p>
              A list of my favorite things:
            </p>
            <ul className="list-disc list-inside grid grid-cols-3">
              <li>Animal: Rhino</li>
              <li>Bodybuilders: The Noble Natties</li>
              <li>Drink: Gin Old-Fashioned</li>
              <li>Exercise: Bulgarian Split Squats</li>
              <li>Food: Sushi, Hotpot, Indian/Thai Curry</li>
              <li>Music Genres: Electronica, Neoclassical, Metal</li>
              <li>Musician: Susumu Hirasawa</li>
              <li>Philosopher: Friedrich Nietzsche</li>
              <li>Video Game: Okami</li>
            </ul>
          </div>
        </div>
        <p className="mt-5 text-emerald-800 text-xl font-semibold">
          Recent Posts
        </p>
        {latestPosts.length ? (
          <div className="flex space-x-3">
            {latestPosts.map((post) => (
              <article key={post.slug} className="bg-blue-600/80 hover:bg-blue-700 rounded-xl p-5 basis-0 grow">
                <Link
                  href={`/posts/${post.slug}`}
                  className="text-lg font-bold text-white"
                >
                  {post.title}
                </Link>
                <p className="text-white">
                  {post.excerpt}
                </p>
                <div className="text-amber-200">
                  <time>{distanceToNow(new Date(post.date))}</time>
                </div>
              </article>
            ))}
          </div>
        ) : null}
      </Container>
    </div>
  );
}

export default HomePage;