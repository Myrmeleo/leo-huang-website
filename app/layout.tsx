import "./global.css";
import { UserProvider } from '@auth0/nextjs-auth0/client';
import Header from "@/components/header";

export default function RootLayout({
  children, }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <UserProvider>
        <body>
          <Header />
          {children}
        </body>
      </UserProvider>
    </html>
  )
}