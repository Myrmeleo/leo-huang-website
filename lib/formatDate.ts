import formatDistanceToNowStrict from "date-fns/formatDistanceToNowStrict";
import format from "date-fns/format";

//convey date of event as a certain number of a certain unit away from now
export function distanceToNow(dateTime: number | Date) {
  return formatDistanceToNowStrict(dateTime, {
    addSuffix: true,
  });
}


//format date for blog pages
export function blogDateFormat(dt: number | Date) {
  return format(dt, "dd MMMM yyyy");
}