import type { Post } from "@/interfaces/interfaces";
import fs from "fs";
import { join } from "path";
import matter from "gray-matter";

const postsDirectory = join(process.cwd(), "_posts");

//read all file names in the dir including subdirectories
export function getPostSlugs() {
  return fs.readdirSync(postsDirectory);
}

export function getPostBySlug(slug: string, fields: string[] = []) {
  //remove file extension
  const realSlug = slug.replace(/\.md$/, "");
  const fullPath = join(postsDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");
  //see public/blog-template for required format
  //data is the set of fields between the dashed lines, content is what comes after
  const { data, content } = matter(fileContents);

  const items: Post = {};

  //Give only the requested fields of the Post
  fields.forEach((field) => {
    if (field === "slug") {
      items[field] = realSlug;
    }
    if (field === "content") {
      items[field] = content;
    }
    if (typeof data[field] !== "undefined") {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllPosts(fields: string[] = []) {
  const slugs = getPostSlugs();
  const posts = slugs
    .map((slug) => getPostBySlug(slug, fields))
    //sort posts by date in descending order
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
  return posts;
}

//get the n most recent posts (all, if less than n)
export function getLatestPosts(n) {
  const posts = getAllPosts(["slug", "title", "excerpt", "date"]);
  const latest = posts.length >= n ? posts.slice(0, n) : posts;
  return latest;
}