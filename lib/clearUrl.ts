//take the origin and path from a URL, removing any extra junk
const clearUrl = (url: string) => {
  const { origin, pathname } = new URL(url);
  return `${origin}${pathname}`;
};

export default clearUrl;
