import IORedis, { Redis } from "ioredis";

//Ensure only one redis exists for the whole program
//This might backfire when I want to scale the website up, but I'll deal with that later
class ClientRedis {
  static instance: Redis;

  constructor() {
    throw new Error("Use Singleton.getInstance()");
  }

  static getInstance(): Redis | null {
    if (!ClientRedis.instance) {
      ClientRedis.instance = new IORedis(process.env.REDIS_URL!);
    }
    return ClientRedis.instance;
  }
}

export default ClientRedis.getInstance();
