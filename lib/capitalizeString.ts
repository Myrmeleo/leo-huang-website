const wordsToLower = ['a', 'an', 'and', 'as', 'at', 'of', 'on', 'or', 'the', 'to'];

//captialize the words in a string according to my best understanding of title conventions
export default function capitalizeString(str : string) {
    const words = str.split(' ');
    const capWords = words.map((w) => {
        const ind = words.indexOf(w);
        const isFirst = ind === 0 || words[ind-1].endsWith(':');
        if (wordsToLower.includes(w) && !isFirst) {
            return w;
        }
        else {
            return w.charAt(0).toUpperCase() + w.slice(1);
        }
    })
    return capWords.join(' ');
}