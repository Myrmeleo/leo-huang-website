import markdownit from 'markdown-it';

//convert a markdown-formatted string to a HTML-formatted string with equivalent content
export default async function markdownToHtml(markdown: string) {
  const parser = markdownit()
    .use(require('markdown-it-collapsible'))
    .use(require('markdown-it-div'))
    .use(require('markdown-it-footnote'))
    .use(require('markdown-it-multimd-table'), {
      multiline:  true,
      rowspan:    true,
      headerless: true,
      multibody:  true,
      autolabel:  true,
    });
  const result = parser.render(markdown);
  return result;
}
