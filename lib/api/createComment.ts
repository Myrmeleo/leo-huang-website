import type { Comment } from "@/interfaces/interfaces";
import redis from "@/lib/redis";
import { nanoid } from "nanoid";
import clearUrl from "@/lib/clearUrl";
import { getSession } from "@auth0/nextjs-auth0";

//add a new comment to the redis
export default async function createComment(
  req: Request
) {
  const key = req.headers.get('key');
  const text = (await req.json()).toString();

  if (!text) {
    return new Response("Missing comment.", {status: 400});
  }
  if (!redis) {
    return new Response("Failed to connect to the redis database.", {status: 500});
  }

  try {
    const session = await getSession();
    if (!session) return new Response("You are not a validated user.", {status: 400});
    const user = session.user;
    const { name, picture, sub, email } = user;

    //create comment with unique id
    const comment: Comment = {
      id: nanoid(),
      created_at: Date.now(),
      storage_key: key,
      text,
      user: { name, picture, sub, email },
    };

    //write data; redis entry is JSON string
    await redis.lpush(key, JSON.stringify(comment));
    return new Response("Successfully posted!", {status: 201});
  } catch (_) {
    return new Response("Unexpected error occurred while deleting comment.", {status: 400});
  }
}
