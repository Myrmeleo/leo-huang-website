import type { Comment } from "@/interfaces/interfaces";
import redis from "@/lib/redis";
import clearUrl from "@/lib/clearUrl";

//get all comments from the redis as a JSON string
export default async function fetchComments(
  req: Request
) {
  const key = req.headers.get('key');

  if (!redis) {
    return new Response("Failed to connect to the redis database.", {status: 500});
  }

  try {
    // get data
    const rawComments = await redis.lrange(key, 0, -1);

    // string data to object
    const comments = rawComments.map((c) => {
      const comment: Comment = JSON.parse(c);
      //deleteComment will not work without comment data matching database row exactly; will fix later
      //delete comment.user.email;
      return comment;
    });
    const jsonComments = JSON.stringify(comments)
    return new Response(jsonComments, {status: 200});
  } catch (_) {
    return new Response("Unexpected error occurred while fetching comments.", {status: 400});
  }
}
