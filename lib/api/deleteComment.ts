import type { Comment } from "@/interfaces/interfaces";
import redis from "@/lib/redis";
import clearUrl from "@/lib/clearUrl";
import { getSession } from "@auth0/nextjs-auth0";

//delete a comment from the redis
export default async function deleteComment(
  req: Request
) {
  const key = req.headers.get('key');
  const comment = await req.json() as Comment;

  if (!comment) {
    return new Response("Missing comment.", {status: 400});
  }
  if (!redis) {
    return new Response("Failed to connect to the redis database.", {status: 500});
  }

  try {
    const session = await getSession();
    if (!session) return new Response("You are not a validated user.", {status: 400});
    const user = session.user;

    const isAdmin = process.env.AUTH0_ADMIN_EMAIL === user.email;
    const isAuthor = user.sub === comment.user.sub;
    if (!isAdmin && !isAuthor) {
      return new Response("You are not authorized to delete this comment.", {status: 400});
    }

    //delete; this method requires comment to match the entry on the redis exactly; I'll find a way to change this later
    await redis.lrem(key, 0, JSON.stringify(comment));
    return new Response(null, {status: 204});
  } catch (err) {
    return new Response("Unexpected error occurred while deleting comment.", {status: 400});
  }
}
