---
title: "Cosmic Significance"
excerpt: "Most people have at some point felt insignificant when viewing their lives on the scale of the universe. But why is this, and why should it matter?"
date: "2024-03-14"
---

&emsp;My passion for philosophy sprouted during the early years of my undergraduate degree, but I was acquainted with the popular 'philosophical' questions that laypeople ponder over well before that. These were the questions that virtually everyone thinks about sometimes in moments of leisurely introspection. The answers they arrive at, I imagine, shape their personality and decisions thereafter in ways I can't precisely describe. What is the meaning of life? Do we have free will? What is the self? But there's one particular issue that causes quite a bit of distress to many who ponder it.

\
&emsp;Let's consider the universe around us. You already know the facts. The Earth being over 4 billion years old, with uncountably many humans having lived and died in the past. We too will die someday, our lifespan over like a flash of lightning, and uncountably many more creatures will come after us. Each person struggles for their own goals; they have feelings, thoughts, and experiences. But only a microscopic fraction of all who lived remain in the collective memory of today. And despite all the clamour going on that might seize our whole attention, our Earth is just a dot in the vast universe populated by countless stars and galaxies. Someday it'll be destroyed, its own life as minuscule compared to the universe's as ours is to its own.

\
&emsp;Well, that's that. Ever since I discovered these truths in my youth, I went on with my life with all of them stored in the back of my head, never thinking much of them. These were just mundane truths with no bearing on my life at all. I thought most others felt the same way, so I was intrigued when I encountered people who made the same discovery as myself but had a much more negative reaction. Their reaction was to question their life's purpose, to doubt everything they used to do with so much conviction, and to diminish their own value. And I was puzzled as to why they did this. I wish I could point to specific examples, but there are so many that they meld together in my mind. You can find posts on r/askphilosophy from Redditors seeking answers to their existential grief, and the general sentiment is found often enough in movies, books, and shows. I suspect the popularity of Existentialism, and of religion at large, is partly a reaction to the state of the universe.

\
&emsp;Let's get a clearer picture of what's going on with these people. What exactly are they thinking? I see 3 patterns of thought that may be expressed in a question each, and I'm going to answer them.

&emsp;
### "What's the point of doing X if it all ends the same way?"

\
&emsp;X could be anything, and by "it all ends the same way" the inquirer often refers to the extinction of humanity or the hypothetical heat death of the universe. And my answer is simply this: If you want to do X, or if it achieves something you care about, that's a perfectly good reason to do it!

\
&emsp;Honestly, it's strange to think that the inevitable future state of the universe should have any bearing on whether or not we take a certain action, more so with respect to whether or not we go on living. I can’t psychoanalyse these people, but I can offer a hypothesis. Perhaps they think that every little action must be for the sake of some greater action, and so on until our very lives exist for some grand purpose. And when such a purpose is found to not exist, or our lives make no difference "in the end", the point of everything is defeated.

\
&emsp;But as Thomas Nagel[^1] pointed out, this is a nonsensical way to deliberate about our lives. Consider first the fact that we can do something for its own sake. Why do I seek knowledge and wisdom? Because I just think these are good things to have. There are other reasons, like helping others live well, but even if these reasons vanished I’d continue on my quest all the same. There’s no need for me to end a global conflict or achieve Nirvana in order for me to justify it.

\
&emsp;Even if something is not done for its own sake but only for the sake of something else, which may in turn be only for the sake of another thing, such a chain of justification is not infinite. (Could we ever be satisfied by an infinite chain?) And there need not be some grand purpose that serves as the ultimate link connecting every single chain running through our lives. In fact, chains of justification are often short, and they’re numerous. Why did I visit the ice cream parlour? Because it makes me happy. And I seek happiness for its own sake. Done and done.

\
&emsp;The “it all ends the same way” mindset seems more ridiculous the more mundane cases we point out that directly contradict it. No one in the audience of a beautiful opera thinks their experience is worthless because they’ll be bored tomorrow. No one eating a delicious dinner thinks they may as well starve because they’ll go hungry again later. 

\
&emsp;There is a general principle at play here: The fact that the end point of a journey is invariable doesn’t change the fact that the journey we take matters. Because the journey, and every goal we seek for its own sake on the way, is valuable in itself. We’re going to die no matter what; that doesn’t mean we shouldn’t live well.

\
&emsp;And the inability of some people to accept this viewpoint has led to some truly bizarre conclusions. Consider William Lane Craig[^2], a Christian philosopher who claimed that because (according to atheism) humanity will perish with no afterlife, every bit of suffering and every single atrocity ever committed by humans “doesn’t really matter”. It's really fascinating to see the ways religious people grapple with the existential questions in this essay.

&emsp;
### "Do we matter?"

\
&emsp;I figure the word “matter” here carries an evaluative meaning. So, are we valuable? Are our lives worth something? It’s easy to take a simple subjectivist view of the situation and say that your life is valuable so long as you value it. But this likely won’t do much to help the existentially anguished inquirer, because their opinion of themselves is affected by their knowledge of the facts I recited at the start. But why would anyone think less of themself because they’re one of billions of people who have lived and will be forgotten, or because they’re outsized and outlived by the cosmos?

\
&emsp;Here’s another hypothesis. A normal human will often seek feedback from others to form their opinion of themself. But we can clearly see we’re not valuable from the indifferent perspective of the *universe*. Any Existentialist will tell you that we weren’t created on this planet for a purpose. There is so much sadness, harm, and injustice going on in the world, and as far as I know, nobody is looking at us from beyond with compassion. From the all-encompassing perspective, one may be drawn to being as indifferent towards their own life as the universe is.

\
&emsp;It's reasonable to ask what good it does to judge things from the universal instead of personal point of view. I don't think it provides any closer access to truth, any greater credence for belief. Now the universal point of view is useful as a therapeutic tool for orienting our mindset in a healthy way when we are stirred up in emotion. If you face difficulty in life and are brought to panic or despair, taking a distant third-person view of your situation can help you conclude that your problems are not as catastrophic and insurmountable as you thought. This is a healthy application of the universal view. But an unhealthy one is when such a view is used to devalue the things you used to value, especially things that given some thought in the personal viewpoint, you cannot deny are worth something after all.

\
&emsp;I say one should independently decide what to think on personal matters such as their self-image; this is a skill that can be trained. I don't mean to imply that one can ever have a thought that is not in some way influenced by the world around them. I just mean that our thoughts should follow logically from the ideals we hold with conviction, which defines the kind of person we are. And even if we lack this ability, we usually have the support of our loved ones to give us that positive outlook. But when this isn't enough for someone it’s likely because they crave confirmation that their life is *eternally* or *objectively* valuable.

\
&emsp;Concerning eternity, there is a common desire in humanity to leave a legacy, to be admired for generations. At a deeper level I detect a common intuition in the existential sufferers that something must matter on an everlasting temporal scale in order to matter at all. See the answer to the previous question. As for objectivity, some people think that objective value is in some sense more worth caring about than subjective value. If one is only satisfied with being objectively valuable, no amount of affirmations, the expressions of human opinion, will satisfy them.

\
&emsp;A popular solution to both problems is to convince oneself that something “above and beyond” humanity confirms that human lives are valuable. This can be the decree of a loving god, a platonic form, or any other supernatural metaphysics that give a foundation for such values. Such foundations are claimed to last forever; your god will always remember you, or your reward for being a good person will be eternal. A religious person may have the intuition that this foundation gives the strongest reason, maybe the only compelling reason, for affirming our lives. 

\
&emsp;I prefer we all stop obsessing over eternity and objectivity. The badness of a fever doesn't vanish just because it's cured later; it remains true that the fever was bad. And while there are plenty of philosophical accounts of how objective value can exist without gods, I am content to be loved by the people I love. We'd be better off as a species learning to create our own values instead of grasping for ones that transcend us.

&emsp;
### "Are we significant?"

\
&emsp;The meaning of “significant”, which I take to be synonymous with “special”, needs clarification. Sometimes the inquirer just means “valuable”, making this a rephrasing of the previous question. But Guy Kahane[^3] has pointed out another meaning of the word that massively changes the answer. To be significant is to be both valuable and *exceptional* in some way. Judgements of significance each have a particular scope, a domain of entities that are being compared to each other. If I'm a grade A student, but the classroom is full of grade A students, I'm good, but not exceptional among the class, so I'm not significant. But if our class is the only one full of grade A's, then we're significant among the school.

\
&emsp;A feeling of cosmic insignificance arises when one who affirms their own value nonetheless feels that, on the cosmic scale, they are not exceptional compared to all the stars, planets, nebulas, quasars, black holes, and supernovas scattered about. We have an immeasurably large competition to stack up to. But in terms of what, exactly? I suppose we're being outdone in terms of beauty, size, and power. But there is an important respect in which we excel above the rest: intelligence. We humans (and animals and potential aliens) are uniquely endowed with the capacity to think and decide. In this sense, we *are* incredibly significant. This is not even mentioning how much of the universe is comprised of empty space. So if we add that to the equation, we stand out even more. 

\
&emsp;In short, the fewer intelligent species there are taking up space, the more significant we are. Now some people will be pleased to hear this answer, knowing that in some way we occupy a special position in the universe. We can adopt the universal point of view and still mark ourselves for distinction. But far from being relieved at this news, I'm pretty dismayed. We are significant in our intelligence precisely because there is almost no intelligent life out there. Every failed search for extra-terrestrial intelligence is a tremendous prospect of intellectual advancement and cultural exchange washed away. And in the wake of disappointment, the stroke of ego from being special is a poor consolation. (Besides, being special doesn't even make me feel good about myself; it's not something I *earned* through hard work.)

\
&emsp;My own philosophy says that a universe is more valuable if it contains more valuable beings. And it's more beautiful with more creatures that engage in aesthetic creation like architecture, music, or poetry. But the barrenness of the cosmic vacuum suggests that the beauty and value of the universe is far less than I wish it were. In fact, how exceptional we are correlates negatively with how close we are to living in the ideal universe. I therefore wish that the level of intelligence we possess was *not* exceptional, and we were *not* cosmically significant in this way. Because a universe in which we're exceptional is strictly worse than one saturated with species like ours, which diminish our significance but provide more beauty and moral value. I'd rather live in the Star Wars Milky Way than our real one, even if I'd be insignificant. Simply being valuable is all I want for myself.

&emsp;
### Why Philosophy Might Not Be the Answer

\
&emsp;At the end of the day, even if you agree with all of my reasoning, you may still feel an uneasiness that you can't shake off and can't justify. This is normal. Existential anxiety is a feeling that doesn't simply go away when you introduce logical arguments into the same brain. It goes away as you live a happy life immersing yourself in the world. If it persists and has a constant detrimental effect on your ability to live well, consult a mental health professional.

\
&emsp;This exemplifies the reason that a philosopher can never be a substitute for a therapist. Philosophy is an endeavour that uses logical arguments to support theories about the world; it is rational by nature. But therapists deal with the *irrational* side of the human brain. These disorders that one did not obtain through argumentation, likewise cannot be removed through argumentation. You need methods other than arguments to cope with such issues. Spend time with friends, open your heart to people you trust, and lift some heavy-ass weights.

\
&emsp;It's true that one's philosophical views can have a profound effect on the attitudes and emotions they have. The simple reason is that there's a causal connection between our beliefs about the world and our feelings towards it. But philosophy cannot fix everything in a broken mind. Hopefully I helped some of you that needed relief. For those still wanting, I wish you the best of luck.

\
&emsp;
[^1]: 1 | Nagel, Thomas. 1970. "The Absurd". Journal of Philosophy 68: 716–27.
[^2]: 2 | [https://youtu.be/Rm2wShHJ2iA?si=XJG8Cmej-TyjxvnA&t=3217](https://youtu.be/Rm2wShHJ2iA?si=XJG8Cmej-TyjxvnA&t=3217)
[^3]: 3 | Kahane, Guy. 2014. "Our Cosmic Insignificance". Noûs 48(4): 745–772. [https://doi.org/10.1111/nous.12030](https://doi.org/10.1111/nous.12030)