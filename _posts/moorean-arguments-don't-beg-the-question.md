---
title: "Moorean Arguments Don't Beg the Question"
excerpt: "Possibly the most simple-yet-controversial argument in philosophy."
date: "2024-04-18"
---

&emsp;My favorite philosophical argument of all time is probably Moore’s argument for the existence of the external world. Behold: to prove the existence of the external world, it suffices to prove the existence of an external object - an object that exists independently of our experience. 

\
&emsp;So Moore raised his hands and argued like this: 

&emsp;1. Here is one hand.\
&emsp;2. Here is another.\
&emsp;3. There is an external object (two of them, in fact).\
&emsp;4. There is an external world.

\
&emsp;I don’t like the argument because I find it particularly compelling, clever, or daring. I like it because of the juxtaposition between how much controversy it’s caused among philosophers and how strikingly simple it is. The body of literature arguing over just how good this argument is, even how exactly to interpret the argument in the first place, is absolutely massive for only four propositions. 

\
&emsp;And Moore has inspired thinkers after him to contribute to a family of similarly structured arguments for claims in other areas of philosophy. Here’s a Moorean argument for the existence of objective moral values:

&emsp;5. Torturing puppies for fun is bad.\
&emsp;6. Feeding starving children is good.\
&emsp;7. There is a moral fact (two of them, in fact).\
&emsp;8. Moral realism is true.

\
&emsp;There are so many criticisms of Moorean arguments in general that I can’t possibly address all of them. Many of them apply to specific interpretations of Moorean arguments. A grasp of what interpretations are currently considered viable will help us decide which criticisms call for consideration. Kelly 2005 gives a good overview of the landscape[^1].

\
&emsp;Honestly though, I’m not too concerned with finding out what exactly Moore meant when he made his famous argument. I’m more interested in the variety of ways to inject meaning into the words of the argument to change its plausibility and implications. Put another way, the correct historical interpretation of the argument, per se, doesn’t matter to me[^2].

\
&emsp;In this essay I'll spell out my way of thinking about Moore’s argument and the unspoken epistemic principles that underpin it. Then I’ll tackle what’s likely the most common criticism of Moorean arguments - that they beg the question. 

&emsp;
### My Spin On Moorean Arguments

\
&emsp;Let’s say you’re trying to confirm or deny the existence of a hypothesised animal species, “Animal-X”. You have a list of necessary and sufficient properties a thing must have to count as an Animal-X (purple fur, 7 metres tall at maturity, etc.). You look far and wide, and see a creature that has all the criteria on your list. You’ve found an Animal-X.

\
&emsp;And what’s more is that, by confirming the existence of an Animal-X, you directly and without further ado confirm the existence of Animal-X the species.

\
&emsp;What exactly is this inferential move that is made directly and without further ado? It’s the inference of the existence of a kind of thing from the fact that there exists a particular thing that fits into that kind. Call this *instance-to-kind inference* (IKI)[^3]. To prove that a kind of thing exists, determine a condition something must fulfill to be reasonably ascribed membership in that kind, and find some particular thing that fulfils the condition.

\
&emsp;This rule of inference should be nothing new to you. It’s used in every domain of intellectual inquiry ever. A historian confirms the past existence of a clan when they find skeletons clothed in the insignia of that clan. And a mathematician confirms the existence of undecidable languages when they find a language that can’t be decided by a Turing machine.

\
&emsp;A metaphysicist confirms the existence of external objects by finding an external object. And a meta-ethicist confirms the existence of moral facts by finding a moral fact.

\
&emsp;This is the essence of Moorean arguments that I propose. An argument in this family is an application of IKI. It basically says, “This thing exists. This thing is an X. Therefore this kind of thing (X’s) exists.” 

\
&emsp;I’ll present Moore’s argument another way to make the connection clear: 

&emsp;9. This hand exists (gestures at hand).\
&emsp;10. This hand is external.\
&emsp;11. The kind external object exists.\
&emsp;12. There is an external world.

\
&emsp;Compare this presentation to the original. Proposition 9 here is expressed as P1 and P2 above. P10 is rephrased as P3. P11 is not made explicit, but it’s supposed to follow from P10 and entail P12. And P12 is P4.

\
&emsp;Hopefully you can see that the logical steps from P10 and P11 and then P12 are reasonable enough, and you won’t deny P9. But what about the support for P10? That doesn’t seem to be supported at all! How can we possibly get there from just P9?

\
&emsp;Moorean arguments are a subset of the set of all arguments that use IKI. And the feature that distinguishes them from the rest is that the move from the premise that “Here is this object” to the premise that “This object has the qualities necessary for membership of the kind in question” is made entirely through intuition. My intent is that from the appearance of a hand, one intuits that they’re looking at an external object. From the consideration of the moral proposition “Feeding a starving child is good”, one intuits that they’re thinking about a moral fact.

\
&emsp;The appeal to intuition, a part of thinking that Moore was (IMO way too) fond of, is probably the source of most controversy surrounding the argument. Intuitions vary heavily among philosophers, let alone lay people, and even when one’s intuition agrees with the premises, they can argue that intuitions don’t hold much if any epistemic weight. So there’s a good chance that a critic’s intuition won’t carry them from the first premise to the second. This is fair enough.

\
&emsp;But as I said earlier, the only critique of Moorean arguments I want to address is that they allegedly beg the question. Now is the lack of intuitive connection between the first two premises what this criticism refers to? No, I think that “begging the question” means something quite different than “this premise has no compelling support and I think it’s false”. If my friend says they’ve proven the existence of linear-time sorting algorithms, they provide an example of such an algorithm, and on observation I think it actually runs in O(nlogn), I would say, “This isn’t an example of the kind of thing you’ve allegedly proven. It doesn’t support your conclusion.” I would not say “You’re arguing in circles, you’re presupposing the claim you set out to prove”. It sounds like I’ve misidentified what’s wrong with my friend’s reasoning.

\
&emsp;It’s hard to precisely identify what feature of an argument a given person thinks is characteristic of begging the question without their elaboration, because the term is ambiguous. In fact, Jim Pryor has identified five ways this criticism can be interpreted[^4].

&emsp;
### What Does It Mean To Beg the Question?

\
&emsp;Charges of question-begging are often associated with other descriptors like “the premise depends on the conclusion”. There are five types of “dependence” between premise and conclusion of an argument that we’ll examine. As we’ll see, not all of them are objectionable.

\
&emsp;**1. If the conclusion were not true, the premise would not be true.** Equivalently, anyone who denies the conclusion would deny the premise.

\
&emsp;This is a characteristic of all deductive argumentation. Unless we want to do away with deductive reasoning, I think we have to make peace with dependence of type 1.

\
&emsp;**2. If the conclusion were not true, there’d be no justification for the premise.**

\
&emsp;I’m not sure Moorean arguments display this type of dependence, but whatever. Pryor gave an example of an argument that ends in “some beliefs are justified”, which will by logical necessity have this dependence. But it looks perfectly fine to me, and I bet you can find other examples of such arguments if you look around. So type 2 dependence is likely not bad.

\
&emsp;**3. If there was no justification for the conclusion, there’d be none for the premise.**

\
&emsp;This type of dependence is had by arguments where the logical connection between the premise and conclusion is so obvious that your source of justification for the former is identical to your source of justification for the latter.

\
&emsp;Suppose I want to construct an argument for the existence of a sandwich on the plate, so I look at the plate and say the following:

&emsp;13. There are two slices of bread stacked on the plate.\
&emsp;14. There is a slice of meat between the slices of bread.\
&emsp;15. A slice of meat stacked between two slices of bread makes a sandwich.\
&emsp;16. There is a sandwich on the plate.

\
&emsp;The source of justification for both premises and conclusion is my eyesight. I look at the plate and grasp the premises, and at the same time I grasp the conclusion. If I lacked justification for the conclusion - if I couldn’t see the sandwich - I would lack justification for the premises. We have a case of type 3 dependence. And yet, I think this argument is perfectly respectable.

\
&emsp;Type 3 dependence is had by arguments for which, once you grasp the premise, you can, directly and without further ado, arrive at the conclusion. That is to say, all arguments that apply the principle of IKI will have this dependence. I don’t think this is a problem. It’s simply a consequence of the logical connection in IKI being incredibly obvious.

\
&emsp;**4. Any evidence against the conclusion weakens the justification one can have for the premise.**

\
&emsp;This is certainly a property of Moorean arguments, but is it a problem? Based on what Pryor says, take the example of a person who has the subjective experience of a physical sensation (say, a bug crawling on their leg). They conclude that there is indeed a bug on their leg, so they’re not hallucinating. Evidence that there’s no bug would undermine the reliability of their introspection and sense of touch.

\
&emsp;I find the reasoning about the bug on the leg to be fine. Those who disagree, thinks Pryor, may be treating the conclusion that there’s a bug conservatively - they think that one needs to be justified in believing there’s a bug before they’re justified in thinking they’re really feeling a bug. But instead you can treat the conclusion liberally - all one needs is a lack of defeaters for the conclusion, and their introspection is sufficient to give justification for the premises. This is the approach I favor. It’s hard for me to argue for liberal over conservative treatment of beliefs without appealing to intuition, so I leave it up to Pryor.

\
&emsp;**5. Justification for the conclusion is a prerequisite for being justified in believing the premise.**

\
&emsp;I understand “prerequisite for justification” to mean this: any possible compelling argument for the premise will, as a premise of its own, have the conclusion. Justification for the conclusion must come prior to justification for the premise.

\
&emsp;It’s not type 5 dependence when an argument for the premise merely supports the conclusion and the premise equally well - that premise and conclusion are justified at the same time. This would just be an example of type 3. And it’s not enough that *some* argument for the premise contains the conclusion. It must be that *all* avenues of justification for the premise require one to first accept the conclusion.

\
&emsp;Type 5 dependence does seem to be an epistemic flaw in an argument. Consider:

&emsp;17. God possesses all virtues.\
&emsp;18. Honesty is a virtue.\
&emsp;19. God is honest.

\
&emsp;How does one go about justifying P17? The only possibility, as far as I know, is to go through all the values and see if God has each of them. But in the process of doing so, when we examine the virtue of honesty and find God has it, we justify P19 before we’ve attained justification for P17. So this argument is suspect, and it’s suspect because it had type 5 dependence.

\
&emsp;When we treat the conclusion of an argument conservatively, cases of type 4 dependence are cases of type 5 dependence. So conservative treatment leads to wholesale rejection of Moorean arguments. But liberal treatment allows us to accept them as merely type 4 dependent arguments.

\
&emsp;So at the end of the day, charges of begging the question can probably be traced to one of the types of dependence listed here. Only type 5, in my opinion, might be a problem with a Moorean argument. But as long as we treat the conclusion liberally, there’s nothing to worry about.

\
&emsp;
[^1]: 1 | Kelly, Thomas. 2005. “Moorean Facts and Belief Revision, or Can the Skeptic Win?”. Philosophical Perspectives 19 (2005): 179-209. [https://doi.org/10.1111/j.1520-8583.2005.00059.x](https://doi.org/10.1111/j.1520-8583.2005.00059.x)
[^2]: 2 | Can it still be called a Moorean argument if the intent and underlying principles are vastly different from Moore’s? Maybe not, but I’ll keep calling it that because the argument’s words and structure remain constant.
[^3]: 3 | To say that a kind of thing exists, on one account, is just to say that there exists a thing that fits into that kind. In that case IKI yields an analytic truth.
[^4]: 4 | Pryor, James. 2004. “What’s Wrong With Moore’s Argument?” Philosophical Issues 14 (2004): 349-378. [https://www.jstor.org/stable/3050634](https://www.jstor.org/stable/3050634)