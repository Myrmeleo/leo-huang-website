---
title: "A Bodybuilding Program For Beginners"
excerpt: "Let's start your journey to become HUGE!"
date: "2024-03-27"
---

I've decided to help others become aesthetic and fit, and my first step is to provide a program for new lifters.

&emsp;
::: #first-3-workouts class=flex
::: #push-workout className=table
#### **Day 1: Push**

| Exercise  | Sets | Reps  | 
| :-----------: | :-----------: | :-----------: |
| Push-Ups  | 2-3  | AMRAP  |
| Overhead Press  | 2-3  | 8-12  |
| Standing Bicep Curl  | 3  | 10-15  |
| Sit-Ups  | 2-3  | AMRAP  |
| Rapid-Fire Band Pushdown  | 1  | 70-100  | \
| - (Elbow Recovery)  |  |  |
:::
::: #space
&emsp;&emsp;&emsp;
:::
::: #legs-workout
#### **Day 2: Legs**

| Exercise  | Sets | Reps  | 
| :-----------: | :-----------: | :-----------: |
| Sled Drag + Push  | 1  | 20m each way  | \
| - (Knee Warmup)  |  |  |
| Box Squats  | 2  | 5-8  |
| Back Extension  | 2-3  | AMRAP  |
| Standing Calf Raise  | 3  | 10-15  |
| Finger+Wrist Curl  | 3  | 10-15  |
:::
::: #space
&emsp;&emsp;&emsp;
:::
::: #pull-workout
#### **Day 3: Pull**

| Exercise  | Sets | Reps  | 
| :-----------: | :-----------: | :-----------: |
| Pull-Ups  | 2-3  | AMRAP  | \
| - (or Chin-Ups)  |  |  |
| Barbell Shrug  | 2-3  | 8-12  |
| Overhead Cable Extension  | 3  | 10-15  |
| Face Pull  | 3  | 8-12  |
| Rapid-Fire Band Pushdown  | 1  | 70-100  | \
| - (Elbow Recovery)  |  |  |
:::
:::

&emsp;

::: #last-2-workouts class=flex
::: #lower-workout
#### **Day 4: Lower**

| Exercise  | Sets | Reps  | 
| :-----------: | :-----------: | :-----------: |
| Sled Drag + Push  | 1  | 20m each way  | \
| - (Knee Warmup)  |  |  |
| Romanian Deadlift  | 2  | 6-10  |
| Reverse Nordic Curl  | 2-3  | AMRAP  |
| Hammer Curl  | 3  | 10-15  |
| Skullcrushers | 3  | 8-12  |
:::
::: #space
&emsp;&emsp;&emsp;
:::
::: #upper-workout
#### **Day 5: Upper**

| Exercise  | Sets | Reps  | 
| :-----------: | :-----------: | :-----------: |
| Dips  | 2-3  | AMRAP  |
| Inverted Row  | 2-3  | AMRAP  |
| Lateral Raise  | 3  | 10-15  |
| Neck Curl  | 3  | 15-20  |
:::
:::

&emsp;
### Intro
\
&emsp;It's been almost 3 years since I've started bodybuilding, one of the best decisions I ever made. I've enjoyed greater strength and energy, a physique I love to see in the mirror, and plentiful health benefits making for a longer life. Mentally, I'm more confident in myself, can better tolerate pain, and developed a firm work ethic. My love for bodybuilding extends beyond its effects into the struggle itself. Pushing heavy weights with all my will gives a sublime feeling. And looking at all the tremendous things bodybuilding gifted me, I'm compelled to share them with the world. I want everyone to get into lifting, whether it's bodybuilding, strongman, streetlifting, or something else; they provide similar benefits, and the choice is up to taste. But being physically active is a necessity for a completely fulfilling life.

\
&emsp;I know that 3 years is actually a tiny amount of experience by bodybuilding standards. Most people at my level are painfully incompetent at designing programs, but I on the other hand have already built my personal intermediate program using sound principles, and gotten amazing gains from it. What separates me from them is that I have guidance from intelligent and good-intentioned fitness influencers. I was fortunate to discover them very early in my fitness journey, so now my progress is at a point where other lifters take several years to reach.

\
&emsp;So let this bodybuilding program be my gift to anyone who wants to walk the same path. It's the program I wish I had at the start of my journey. I've designed it with careful consideration of the kind of people who are just starting to be physically active. Such people aften are skinny or obese, have low work capacity, and are wavering in their commitment to the journey, not sure how long they'll stick with it. Starting the path to fitness is to be praised, and sticking with it through tough times makes you incredibly exceptional. Over 90% of lifters never make it past the beginner stage, to their tremendous loss. I'll do my best to keep you motivated, but most of the mental battle comes down to you.

&emsp;
### The Program
\
I've designed the program with these principles at the forefront:
+++ **1: Develop every muscle.**
&emsp;Most beginner programs have glaring holes, meaning that some muscle groups that make a huge impact on aesthetics are not hit by any exercise. This means that when you move on from the program, you have an imbalance between different muscles and the smaller one takes work to catch up. The disparity is greatest in the beginner stage because *your muscles grow the fastest at the start of your journey*. This early period is called *newbie gains*, and it's a wonderful experience. But it's better if you can see everything grow at once.
+++
+++ **2: Master your own body weight.**
&emsp;You should learn the motor skills needed to smoothly move your body through space. You'll be doing calisthenics mixed with free weight exercises. Calisthenics will allow you to perform any motions you might do in your daily life with the greatest of ease, and easily break into any other bodybuilding exercise later on. They're low on fatigue, so you can do lots of reps and grow faster. And you can progress on them forever; you could have the same movements in your program from Day 1 in the gym to age 70. I understand that many of you are obese and will struggle to lift your bodies; so was I when I started. So I'll tell you how to modify the movements for your particular weight and strength level.
+++
+++ **3: Get accustomed to pain and struggle.**
&emsp;By far the most important, yet absent skill in newbies is the ability to grind through pain. Lifting is painful and exhausting, and will often have you questioning if you want to be there. I don't want to discourage you, but be prepared for the internal battle with your mind. The ability to endure pain and give your max effort is a skill that needs to be built. You build it by *going to failure*, which means doing repetitions of an exercise until your muscle can't move the weight all the way even though your mind commands it to. It's crucial that the *muscle* gives out, not that your mind loses the will to continue. To build pain tolerance, I've picked exercises that you can safely go to failure on.
+++

\
&emsp;Now let's discuss the **program structure**. Every program has a *split*, a mapping of particular days to particular muscle groups that will be trained on that day. Popular splits include Push/Pull/Legs, Upper/Lower, Full Body, Arnold (which I'm running), and the Bro Split. I'm putting you on a hybrid split. This will give ample opportunity to hit every single muscle. 
+++ Technical Details
&emsp;A normal Push/Pull day will have you work all the muscles involved in pushing/pulling movements - chest/shoulders/triceps and back/biceps respectively. These are *synergistic* muscles, because they help each other on many of the same movements. But this also means that whenever one of them is exhausted, the next movement likely engages it before it can recover, and it'll limit your performance on the movement, forcing you to stop before the target muscle has been fully worked. For this reason, I prefer to match *antagonistic* muscles on a day. These are involved in totally different lifts, so no interference. biceps will be on Push day, and triceps on Pull.
+++

\
&emsp;Newbie programs will always be low on the exercise count (~6-7 per day) to accommodate for low work capacities, and be distributed throughout only 2-4 days a week. My approach is to have you work out 5 days a week, with only 4 exercises per day. This will allow you to accumulate plenty of volume for growth without taxing you too much in a single day. It'll also get you used to the frequency that you can expect to do after the newbie stage. You'll do each day once a week, with 2 days of rest to distribute how you see fit. I recommend placing one after Pull and one after Upper.

&emsp;
### You CAN Do Calisthenics!
\
&emsp;I've selected movements that are easy to learn, safe to take to failure, and fun to do. I won't teach you here which muscles each exercise is meant to target or what the ideal technique is; you can find all this info online. But because of the variety of bodies people have going into this program, with many being overweight to obese, some movements may not be accessible to everyone without modification. So if you're exceedingly heavy like I was, here's some tips on making the most of these exercises.

+++ Push-Ups
&emsp;When I started out, I was so fat and weak I couldn't do a single bodyweight push-up. What I should have done instead of moving to dumbbell presses was the *incline* push up. Place your hands on a point above your feet. The higher they are, the easier the movement. Make sure there's enough friction between your feet and the floor (I use rubber mats). Now do your push-ups from that angle.
\
&emsp;As you get stronger, you lower the height of your hands to progress the difficulty. There's a few tools to use as an adjustable holding point. Some people use the back rest on an incline bench. I don't like this because the narrow bench forces you to use a close grip, shifting much of the work onto the triceps instead of the pecs. I recommend the doorway pull-up bar, the kind that uses suction to attach to the inside of a door frame. Invest in one that can sustain a lot of weight.
+++
+++ Sit-Ups
&emsp;Similar to push-ups, starting from an incline on a bench is recommended. You can lower the angle of the bench as you progress, eventually going into a decline. Add weight only when you're at the maximum decline, and only in very small increments (2.5-5.0lbs). Holding the weight above or behind your head will make it way harder to lift than holding it to your chest.
+++
+++ Back Extension
&emsp;The strength curve (i.e. change in difficulty of movement over change in joint angle) corresponds to the angle of your legs with the ground. Back extensions are most difficult when your legs are parallel with the ground, moderately difficult at a 45-degree angle, and easiest when they're vertical. Start with your legs vertical, then shift the angle of their legs downward as they progress.
+++
+++ Pull-Ups
&emsp;These are exponentially more difficult to do the heavier you are, but easy to modify. Find something with an adjustable height to grip onto, such as a doorway pull-up bar or a pair of rings. Most gyms will have an assited pull-up machine, which doubles as a dip machine. Otherwise, here are some variations in order from easiest to hardest.
- A. Start with your butt on the ground and your feet close in front of you, with the bar just low enough that you can grip onto it in a dead hang position. Do your pull-ups from this position. Most of the weight of your legs will be diverted onto the ground, so you don't have to lift it.
- B. Perform the Rack Pull-Up. Place an incline bench in front, with the backrest pointed to you and raised high enough to get a good amount of bend in your hips. Holding onto the bar, lift your legs and put the groove of your ankles on the backrest. Do your pull-ups from this position.
- C. Do normal pull-ups, but with a resistance band attached to the bar and looped under your feet. The stronger the band, the more weight is taken off your back.
+++
+++ Reverse Nordic Curl
&emsp;Most beginners lack the joint flexibility to get a long range of motion, and there's a real risk of snapping your knees if you stretch further than they're adapted to. So do these with a resistance band. Attach a band to a pole and do your curls facing the pole and holding onto the band. Go through the negative slowly, reversing course when you feel anything concerning in your knee joint. Your flexibility will improve with practice.
+++
+++ Dips
&emsp;Most gyms have an assisted dip machine. If your's doesn't or someone is using it, your best option is to go banded. Loop a band around both dip handles and stand on the middle. You'll probably need a really strong one at first.
+++
+++ Inverted Row
&emsp;The more vertical your body is, the easier the movement. As with push-ups, set your bar/rings high up to start, then lower it as you progress. Use a bench to raise your legs if you want to get completely horizontal.
+++

&emsp;
### Progression on Calisthenics
\
&emsp;Doing a high number of reps per set on calisthenics is a great way to build cardiovascular endurance, which lets you work harder without being as fatigued. So keep adding reps until you can do 20 or more. When you can do this with the standard variation of a movement, you may think it's time to add weight, such as by holding onto a kettlebell for wearing a backpack. But I suggest you find different ways to make the exercise more difficult. Increase your range of motion. Lower the angle of your body. Use a slower tempo and a pause at the bottom. To master your bodyweight means being able to perform the movements well given any set of variables.

\
&emsp;Add a deficit to your push-ups by putting your hands on a stack of books, weight plates, or parallettes; this puts a massive stretch on your pecs. Do your pull-ups with an L-sit or a wide grip. And when you're really good, use rings. These are very unstable and you'll shake like crazy at first. But your brain will adapt and you'll have a fantastic way to progress for years without adding any weight at all.

&emsp;
### How to Make the Most of This Program
\
**Trust the plan.** A terrible mistake to make when following a program is to ditch it well before it stops bringing gains. Impatiently hopping from one program to the next, seeking something new and exciting, is a surefire way to get nowhere. Muscle growth takes time and consistency, even for a newbie. You need to repeatedly do the same movements again and again, progressing steadily, to ensure that your muscles are being worked harder over time. This progressive difficulty is the driver of growth. Stick to this program for *at least 24 weeks*. Then you can modify it or switch to a more challenging program as it suits you. 

\
**Track your progress.** You must keep a training log to ensure you're gradually progressing in weight and/or reps each week. The increase in the amount of work you do, called *progressive overload*, causes an increase in the stimulus your muscle receives to grow. You can also add an extra set to an exercise when your work capacity can accomodate it. But to know that your performance is getting getter over time, you need a training log. It also helps you decide what performance goal to aim for in your current session; I always try to add a rep over last week.

\
**Take care of yourself outside the gym.** Eat a healthy diet with plenty of protein and vitamins. If you're fat, cutting calories is the quickest way to lose fat, which will improve your work capacity greatly. If you're skinny, you'll need to bulk up in order to grow. Sleep at least 7, ideally 8, hours a day. Keep your stress levels low by doing relaxing hobbies and cutting out social media; exercise itself is a great way to relieve stress. Do lots of cardio, for your heart health and your ability to do more work in the gym.

\
**Believe in your potential.** Most lifters are small, but almost no lifters get anywhere near the size they could naturally build in their lifetime. Because we all make mistakes, yet most don't even realize what they're doing wrong. I can tell you that self-doubt is the biggest killer of potential. Beginners will come into bodybuilding thinking they can't be a massive beast without taking PEDs or having extraordinary genetics. But *you can* get huge naturally - look at the Noble Natties. And talk of genetics is almost always a post-hoc rationalization of the speaker's prior expectations. If someone's big, they have top-tier genetics; surely it couldn't be caused by something else! These are the thoughts of someone who won't succeed because they don't expect greatness for themselves. Break the limits on your own mindset, and unlock the path to excellence.