---
title: "The Philosophy of Miyamoto Musashi"
excerpt: "Life lessons from one of the greatest swordsmen in history."
date: "2024-04-08"
---

&emsp;Miyamoto Musashi was a warrior whose legacy persists mostly in legend. We can say that he was born in 1584 fathered by sword expert Shinmen Munisai, that he left his home village as a young teenager to travel Japan, and that he founded the Ni-Ten Ichi Ryu school of martial arts. Most other claims, even ones supported by the best historical sources, sound larger than life. Supposedly, Musashi killed his first opponent in a duel, named Arima Kihei, at the age of about 12, then killed Tadashima Akiyama at about age 15. He fought in the battle of Sekigahara in 1600, had multiple duels with the Yoshioka clan in Kyoto in 1604, and slew the revered swordsman Sasaki Kojiro in 1612. This would be an incredible track record by itself, not even counting the more speculative events in his life.

\
&emsp;By Musashi’s own account in his Book of Five Rings, he fought in at least 60 duels over his lifetime, not losing a single one. The problem for historians is that few of his 60 opponents are identified by name, and few of those duels are even recorded to have happened, leaving it open whether he was telling the truth. It doesn’t help that public perception of Musashi has been distorted by overtly fictional works such as Eiji Yoshikawa’s Musashi and Takehito Inoue’s manga Vagabond (which are amazing works in their own right).
\
&emsp;

![A portrait of Musashi late in his life. He is sitting cross-legged with a quill in his hand.](/blog/musashi-portrait.png#center-img "Musashi's portrait")

\
&emsp;I’m not here to establish Musashi’s biography, but to analyse his philosophy and see what we can learn. Happily for my purposes, it is certain that Musashi indeed wrote the Book of Five Rings and the Dokkoudou, two texts that contain almost all of the philosophical thoughts he put on paper. Musashi is not a name you’ll hear in school, because his ideas don’t contribute substantially to any ongoing projects or debates in academia. Granted, they’re not particularly deep or revolutionary, and his claims aren’t supported by arguments but by experience. This means his ideas belong exclusively to the scope of the everyday person, who seeks simple practical wisdom that doesn’t need to be profound.

\
&emsp;Let’s go through the two texts of interest, starting with the Book of Five Rings.

&emsp;
### The Book of Five Rings

\
&emsp;Musashi’s penultimate work[^1], especially the last chapter (the “Book of Void”), is dotted with Buddhist metaphysics, of which I have little to say for lack of familiarity. I can say that his practical advice for living and thriving in the world is worth following. On the surface level, most of his teachings concern the art of combat, of fighting stances and striking techniques, which has little application to the modern person. If you look closer though, you’ll see the patterns in his thought and action that brought about his success.

\
&emsp;The first lesson we can learn from Musashi is to tightly couple the concepts of understanding and doing. To understand a teaching is to be able to put it into practice, and one who cannot enact an idea doesn’t understand it. Musashi is by far the most anti-armchair philosopher I have ever read, one who realized that all but the shallowest wisdom will be forever cut off from those in the ivory tower who refuse to engage directly with the world.

\
&emsp;In his introduction, Musashi claims that none of his victories were owing to a mastery of the art of strategy; in fact, he didn’t learn the way of strategy until the age of 50. So how was he so strong? He might not have grasped strategy with his rational mind, but he understood it intuitively and by habit. A proficiency in combat cannot be gained through listening to winding lectures and slogging through textbooks, but from combat itself. Musashi’s grasp of strategy was developing as early as 13 years old, and progressed with each battle he entered. Participation in real combat, and direct confrontation with an enemy is the only way to become a master of strategy.

\
&emsp;We also find an explanation of why Musashi’s opponents were inferior to him. In the first chapter he describes, “Those teaching and those learning the Way are concerned with colouring and showing off their technique”. The masters of the opposing martial art schools impressed their students with flashy demonstrations and flowery speeches that appear profound at surface level but lack substance. None of this actually served to improve the students’ understanding of strategy. And the students themselves were too concerned with looking impressive to devote their time towards becoming great at their art. Musashi’s disdain of the masters makes it little wonder that when he later practised various arts, they were “all things with no teacher”.

\
&emsp;Throughout the entire book, Musashi will frequently end his instructions for combat technique and frame of mind with an admonishment to continue studying the matter by oneself. He gives such snippets as “you must study this carefully”, “you must train”, and curiously, “I cannot explain in detail how this is done”. Is Musashi doing this because he’s a poor communicator or trying to mask his lack of knowledge? Rather, I think it’s because he understood that the teachings he provides must be practised in order to be apprehended, and that life experience is a better teacher of his art than any book could be. And indeed, there are truths in this world that, even if expressible in words, will not be taken to heart by those who haven’t gone through the proper experiences. For me, it was the truth that pain is not bad, one that I would never have believed without my years spent in physical training. 

\
&emsp;Musashi’s blend of action and understanding is such that he instructs the reader to carry out his teachings as a constant part of ordinary life. In the outline of the Fire book, he advises us to “treat training as a part of normal life with your spirit unchanging”. In the water book, he identifies the form you should take in combat with the one you should take in normal circumstances. 
Concerning temperament: “In strategy your spiritual bearing must not be any different from normal. Both in fighting and in everyday life you should be determined though calm.”
Concerning posture: “In all forms of strategy, it is necessary to maintain the combat stance in everyday life and to make your everyday stance your combat stance.”
Concerning gaze: “Learn what is written here; use this gaze in everyday life and do not vary it whatever happens.”
Concerning footwork: “Whether you move fast or slow, with large or small steps, your feet must always move as in normal walking.”
In this way, you are constantly improving, and your best self is the one you present to the world at every moment.

\
&emsp;The second recurring idea in the Book of Five Rings is that one should learn to excel in all situations, and to extend the application of principles from small to large scale situations. One of the most desirable qualities of a warrior is the ability to adapt the spirit to one’s circumstances, as with Musashi’s metaphor of water filling a receptacle. A person who is prepared for anything cannot be stopped from reaching their goals. Musashi outlines his Earth book by revealing the essence of his way of strategy: to “know the smallest things and the biggest things, the shallowest things and the deepest things”. He spends time throughout the book to detail fighting with all sorts of weapons, in all sorts of terrain.

\
&emsp;Musashi’s emphasis on versatility is further stressed in his Water book, in which he expounds on the idea of “having one thing, to know ten thousand things”. The principles used to succeed in conquering one task can be used to conquer the same task many times over. Musashi continually draws comparisons between small battles and massive battles, between one-on-one duels and armies of ten thousand a side, reaffirming each time that the principles of strategy are the same. He insists that if you can confidently win a battle with one man using the way of strategy, you need not fear a battle with thirty men.

\
&emsp;In the current day few of us find ourselves in combat, so the application of combat strategy is narrow. But even when a task differs from a duel so that new principles must be followed, the reader is instructed to get a grasp of whatever principles it requires. Musashi describes the way of the warrior as the joint way of pen and sword, meaning that both physical prowess and intellectual acumen are indispensable. He lays out a set of rules in the Earth book for his students, including to “become acquainted with every art” and “know the ways of all professions”. As for his way of strategy, it was designed to have such a wide range of applicability that one who masters it “will see it in everything”.

\
&emsp;
!["Shrike in a Barren Tree", an ink painting by Musashi.](/blog/kobokumeigekizu.jpg#right-img "Musashi's art of a shrike")

&emsp;
### Dokkoudou

\
&emsp;Musashi’s final text[^2] simply consists of 21 ethical principles, without elaboration or argument. It’s short enough for me to repeat here:

\
&emsp;1. Accept everything just the way it is.\
&emsp;2. Do not seek pleasure for its own sake.\
&emsp;3. Do not, under any circumstances, depend on a partial feeling.\
&emsp;4. Think lightly of yourself and deeply of the world.\
&emsp;5. Be detached from desire your whole life long.\
&emsp;6. Do not regret what you have done.\
&emsp;7. Never be jealous.\
&emsp;8. Never let yourself be saddened by a separation.\
&emsp;9. Resentment and complaint are appropriate neither for oneself nor others.\
&emsp;10. Do not let yourself be guided by the feeling of lust or love.\
&emsp;11. In all things have no preferences.\
&emsp;12. Be indifferent to where you live.\
&emsp;13. Do not pursue the taste of good food.\
&emsp;14. Do not hold on to possessions you no longer need.\
&emsp;15. Do not act following customary beliefs.\
&emsp;16. Do not collect weapons or practice with weapons beyond what is useful.\
&emsp;17. Do not fear death.\
&emsp;18. Do not seek to possess either goods or fiefs for your old age.\
&emsp;19. Respect Buddha and the gods without counting on their help.\
&emsp;20. You may abandon your own body but you must preserve your honour.\
&emsp;21. Never stray from the Way.

\
&emsp;Overall, the work encourages an ascetic and frugal life, and is as pragmatic as Musashi ever was. I suppose there’s a case to be made for asceticism as the path to a peaceful and pleasant life. But I am a man of ambition, and I have the will to chase my goals while enduring any suffering that comes as a result. The ascetic may say that my quest will likely end in failure and the pain of disappointment, but I’d contest that the journey itself is the goal and that the struggle justifies itself through its own nobility. I guess I’m not convinced that the ascetic life is in fact the best life, better than a life spent striving for achievement, regardless of whether that striving actually leads to success. 

\
&emsp;Still, there are principles that are of philosophical interest beyond asceticism. Principle 2 is a direct denial of hedonism, the view that only pleasure is good for its own sake. While I think that pleasure is valuable, I also find that there are many intrinsic goods in the world such as knowledge and freedom, and so hedonism is a narrow-sighted view of the value one has to enjoy in life.

\
&emsp;Principles 9 and 19 both encourage the development of a strong-minded, self-sufficient individual. Resentment is the result of festering anger for some wrongdoing the holder has suffered at the hands of others, and complaining is the action they take when they need something given to them. But the strong individual has no need for resentment, since they can exact justice or prevent wrongdoing, nor complaint, since they do not depend on others to get what they want. Neither do they need to petition for favours from heaven, because the human being is sufficiently powerful to satisfy their own needs.

\
&emsp;Principles 8 and 17 are guiding principles of the way of the warrior, which in Musashi’s eyes is “resolute acceptance of death”. Fear of death causes lasting anxiety that saps a person’s quality of life over the long term. It presents a mental barrier to action, which is especially detrimental for the warrior for whom the action that presents a risk of death also provides the greatest rewards, whether achievement or liberty or honour.

\
&emsp;In these works, we see the principles that made Musashi the legend that he was and still is.

\
&emsp;
[^1]: 1 | Musashi, Miyamoto. 1645. “The Book of Five Rings”.
[^2]: 2 | Musashi, Miyamoto. 1645. “Dokkoudou”.