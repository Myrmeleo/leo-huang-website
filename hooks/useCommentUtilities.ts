'use client';
import type { Comment } from "@/interfaces/interfaces";
import useSWR from "swr";
import React, { useState } from "react";
import { usePathname } from "next/navigation";

//for SWR
const fetcher = (url, pathName) =>
  fetch(url, { headers: { 'key': pathName, } })
    .then((res) => {
      if (res.ok) {
        return res.json();
      }
      throw new Error(`${res.status} ${res.statusText} while fetching: ${url}`);
    });

//get the hook to modify the comment form, the list of comments, and the functions to create and delete
export default function useCommentUtilities() {
  const [text, setText] = useState("");
  const pathName = usePathname();

  //implicity call GET on the API; see api/comment/route.ts GET and fetchComments.ts
  //fetchComments returns a JSON string, which SWR is responsible for parsing
  const { data: comments, mutate} = useSWR<Comment[]>(
    "/api/comment/",
    (url) => fetcher(url, pathName),
    { fallbackData: [] },
  );

  //see api/comment/route.ts POST and createComment.ts
  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      await fetch("/api/comment/", {
        method: "POST",
        body: JSON.stringify(text),
        headers: {
          "Content-Type": "application/json",
          "key": pathName,
        },
      });
      setText("");
      //revalidate to show changes immediately
      mutate();
    } catch (err) {
      console.log(err);
    }
  };

  //see api/comment/route.ts DELETE and deleteComment.ts
  const onDelete = async (comment: Comment) => {
    try {
      await fetch("/api/comment/", {
        method: "DELETE",
        body: JSON.stringify(comment),
        headers: {
          "Content-Type": "application/json",
          "key": pathName,
        },
      });
      //revalidate to show changes immediately
      mutate();
    } catch (err) {
      console.log(err);
    }
  };

  return { text, setText, comments, onSubmit, onDelete, mutate };
}
