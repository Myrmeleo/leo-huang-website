'use client';

import { useUser } from "@auth0/nextjs-auth0/client";
import { useRouter, usePathname } from "next/navigation";
import useCommentUtilities from "@/hooks/useCommentUtilities";

export default function CommentForm() {
  const {text, setText, onSubmit} = useCommentUtilities();
  const {user} = useUser();
  const {replace} = useRouter();
  const pathName = usePathname();
  return (
    <form
      onSubmit={onSubmit}
      className="bg-amber-300/80"
    >
      <div className="pt-3 px-3">
        <textarea
          className="p-3 flex w-full max-h-40 rounded resize-y bg-gray-100 bg-clip-padding placeholder-gray-500"
          rows={1}
          placeholder={
            user
              ? `Got something to say?`
              : "Log in to leave a comment."
          }
          onChange={(e) => setText(e.target.value)}
          value={text}
          disabled={!user}
        />
      </div>
      <div className="flex items-center">
        {user ? (
          <div className="flex items-center space-x-2 m-3">
            <button className="py-2 px-4 rounded bg-amber-600/80 text-white disabled:opacity-40 hover:bg-amber-800">
              Post
            </button>
            <button
              className="py-2 px-4 bg-amber-600/80 text-white rounded disabled:opacity-40 hover:bg-amber-800"
              onClick={() => {
                replace(`../api/auth/logout`);
              }}
            >
              Log Out
            </button>
          </div>
        ) : (
          <div className="m-3">
            <button
              type="button"
              className="py-2 px-4 rounded bg-amber-600/80 text-white disabled:opacity-40 hover:bg-amber-800"
              onClick={() => {
                {/*come back to the current page after login*/}
                replace(`../api/auth/login?returnTo=${pathName}`);
              }}
            >
              Log In
            </button>
          </div>
        )}
      </div>
    </form>
  );
}
