import CommentForm from "./form";
import CommentList from "./list";

export default function CommentSection(user) {
  const isAdmin = user && user.email === process.env.AUTH0_ADMIN_EMAIL;
  return (
    /*The margin fails to appear with various values above 12. I'm not gonna bother figuring out why.*/
    <div className="mt-16">
      <CommentForm/>
      <CommentList userIsAdmin={isAdmin}/>
    </div>
  );
}
