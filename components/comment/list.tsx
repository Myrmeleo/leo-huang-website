'use client';

import { useUser } from "@auth0/nextjs-auth0/client";
import { blogDateFormat } from "@/lib/formatDate";
import useCommentUtilities from "@/hooks/useCommentUtilities";

export default function CommentList({userIsAdmin} : {userIsAdmin : Boolean}) {
  const {comments, onDelete, mutate} = useCommentUtilities();
  const { user } = useUser();
  //revalidate immediately to prevent comments from one page showing on another
  mutate();
  return (
    <div className="space-y-3 mt-5">
      {comments &&
        comments.map((comment) => {
          //used to determine whether the delete button will appear
          const isAuthor = user && user.sub === comment.user.sub;
          return (
            <div key={comment.created_at} className="flex space-x-2 bg-amber-200">
              <div className="flex-shrink-0 m-2">
                <img
                  src={comment.user.picture}
                  alt={comment.user.name}
                  width={40}
                  height={40}
                  className="rounded-full"
                />
              </div>
              <div className="flex-grow">
                <div className="flex space-x-2">
                  <b>{comment.user.name}</b>
                  <time className="text-sky-700">
                    {blogDateFormat(comment.created_at)}
                  </time>
                  {(userIsAdmin || isAuthor) && (
                    <button
                      className="text-sky-700 hover:text-red-500"
                      onClick={() => onDelete(comment)}
                      aria-label="Close"
                    >
                      x
                    </button>
                  )}
                </div>
                <div>{comment.text}</div>
              </div>
            </div>
          );
        })}
    </div>
  );
}
