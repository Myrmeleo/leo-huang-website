import Link from "next/link";
import Container from "@/components/container";

export default function Header() {
  return (
    <header className="py-2 bg-emerald-600">
      <Container>
        <nav className="flex space-x-6 text-white text-lg font-semibold">
          <Link href="/" className="hover:font-bold">Home</Link>
          <Link href="/posts" className="hover:font-bold">Posts</Link>
          <Link href="https://gitlab.com/users/Myrmeleo/projects" className="hover:font-bold">Portfolio</Link>
          <Link href="Leo's Resume.pdf" className="hover:font-bold" download>Resume</Link>
        </nav>
      </Container>
    </header>
  );
}
