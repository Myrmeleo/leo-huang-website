export default function Container({ children }: {children: React.ReactNode}) {
  return (
    <div className="container max-w-100 m-auto px-4">
      {children}
    </div>
  );
}
